# -*- coding: utf-8 -*-
from django.db import models
from joshikousei.alumnos.choices import *
from joshikousei.ubicacion.models import Provincia
from joshikousei.local_apps.models import TIPO_CONTACTO


class Apoderado(models.Model):
    apellido_paterno = models.CharField("Ape. Pat.", max_length=50)
    apellido_materno = models.CharField("Ape. Mat.", max_length=50)
    nombre = models.CharField(max_length=50)
    sexo = models.CharField(max_length=1, choices=GENDER_CHOICES)
    nacimiento = models.DateField()
    dni = models.CharField("DNI", max_length=10, blank=True, null=True)
    
    vive = models.BooleanField("¿Vive?", default=False)
    
    # Si no vive ¿Para qué llenar el resto?
    instruccion = models.CharField("Nivel de Instrucción", max_length=30, choices=NIVEL_INST_CHOICES)
    lug_nacimiento = models.CharField("Lug. Nac.", max_length=50, null=True, blank=True)
    lengua_materna = models.CharField("Lengua Materna", max_length=15, choices=LENGUA_CHOICES, default="Español")
    
    # Trabajo (los datos del trabajo están en el choices de "Contacto"
    ocupacion = models.CharField("Ocupación", max_length=30, blank=True, null=True)
    
    ingreso = models.DecimalField("Ingreso Mensual (S/.)", max_digits=7, decimal_places=2, blank=True, null=True)
    
    observaciones = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u'%s %s %s' % (self.apellido_paterno,  self.apellido_materno, self.nombre)
        
class ApoderadoContacto(models.Model):
    apoderado = models.ForeignKey(Apoderado)
    tipo = models.CharField(max_length=13, choices=TIPO_CONTACTO)
    valor = models.CharField(max_length=200)
    preferido = models.BooleanField()
    
    def __unicode__(self):
        return u'%s - %s' % (self.apoderado, self.tipo)
        
class Alumno(models.Model):
    codigo_educando = models.CharField("Cod. Educando", max_length=20, blank=True, null=True)
    apellido_paterno = models.CharField("Ape. Pat.", max_length=50)
    apellido_materno = models.CharField("Ape. Mat.", max_length=50)
    nombre = models.CharField(max_length=50)
    sexo = models.CharField(max_length=1, choices=GENDER_CHOICES)
    nacimiento = models.DateField()
    
    provincia = models.ForeignKey(Provincia)
    distrito = models.CharField("Distrito", max_length=50)
    
    lengua_materna = models.CharField("Lengua Materna", max_length=15, choices=LENGUA_CHOICES, default="Español")
    lengua_adicional = models.CharField("Lengua Adicional", max_length=15, choices=LENGUA_CHOICES, null=True,  blank=True)
    
    religion = models.CharField("Religión", max_length=50, choices=RELIGION_CHOICES)
    dni = models.CharField("DNI", max_length=10, blank=True, null=True)
    
    def obtenerpath(instance, filename):
        return u'fotos_alumnos/%s' % (unicode(filename))
    foto = models.ImageField(upload_to=obtenerpath, blank=True, null=True, verbose_name="Foto")
    
    observaciones = models.TextField(blank=True, null=True)
    #Parentesco = models.ForeignKey(Parentesco, verbose_name="Parentescos", help_text="Escribir el Apellido del Tutor")
    

    def __unicode__(self):
        return u'%s %s %s' % (self.apellido_paterno,  self.apellido_materno, self.nombre)

    def edad(self):
        from datetime import date
        born = self.nacimiento
        today = date.today()
        try: # raised when birth date is February 29 and the current year is not a leap year
            birthday = born.replace(year=today.year)
        except ValueError:
            birthday = born.replace(year=today.year, day=born.day-1)
        if birthday > today:
            return today.year - born.year - 1
        else:
            return today.year - born.year
        
class AlumnoApoderado(models.Model):
    alumno = models.ForeignKey(Alumno)
    apoderado = models.ForeignKey(Apoderado)
    relacion = models.CharField("Relación", max_length=10, choices=RELACION_CHOICES)
    tutor = models.BooleanField(default=False)
    
    def __unicode__(self):
        return u'%s - %s' % (self.apoderado, self.alumno)