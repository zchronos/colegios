# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from joshikousei.alumnos.models import Alumno, Apoderado, ApoderadoContacto


class AlumnoAdmin(admin.ModelAdmin):
    list_display = ('id', '__unicode__', 'lengua_materna', 'religion',)
    pass

admin.site.register(Alumno, AlumnoAdmin)

class ApoderadoContactoInline(admin.TabularInline):
    model = ApoderadoContacto
    extra = 1

class ApoderadoAdmin(admin.ModelAdmin):
    inlines = (ApoderadoContactoInline,)
    list_display = ('__unicode__',)
    
admin.site.register(Apoderado, ApoderadoAdmin)