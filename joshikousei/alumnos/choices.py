# -*- coding: utf-8 -*-
GENDER_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)

LENGUA_CHOICES = (
    (u'Aymara', 'Aymara'),
    (u'Chino', 'Chino'),
    (u'Español', 'Español'),
    (u'Francés', 'Francés'),
    (u'Inglés', 'Inglés'),
    (u'Portugués', 'Portugués'),
    (u'Quechua', 'Quechua'),
    (u'Ruso', 'Ruso'),    
    (u'Otra', 'Otra'),
)

RELIGION_CHOICES = (
    (u'Budismo', 'Budismo'),
    (u'Cristiana Católica', 'Cristiana Católica'),
    (u'Evangélica', 'Evangélica'),
    (u'Iglesia de Jesucristo de los Santos de los Últimos Días', 'Iglesia de Jesucristo de los Santos de los Últimos Días'),
    (u'Testigos de Jehová', 'Testigos de Jehová'),
    (u'Otra', 'Otra'),
)

RELACION_CHOICES = (
    (u'Padre', 'Padre'),
    (u'Madre', 'Madre'),
    (u'Primo', 'Primo'),
    (u'Sobrino', 'Sobrino'),
    (u'Abuelo', 'Abuelo'),
    (u'Otro', 'Otro'),
)

NIVEL_INST_CHOICES = (
    (u'Primaria Completa', 'Primaria Completa'),
    (u'Primaria No Completa', 'Primaria No Completa'),
    (u'Secundaria Completa', 'Secundaria Completa'),
    (u'Secundaria No Completa', 'Secundaria No Completa'),
    (u'Superior Univ. Completa', 'Superior Univ. Completa'),
    (u'Superior Univ. No Completa', 'Superior Univ. No Completa'),
    (u'Superior No Univ. Completa', 'Superior No Univ. Completa'),
    (u'Superior No Univ. No Completa', 'Superior No Univ. No Completa'),
)


RESPONSABLE_CHOICES = (
    (u'Madre', 'Madre'),
    (u'Padre', 'Padre'),
    (u'Tutor', 'Tutor'),
)


DOC_CHOICES = (
    (u'DNI', 'DNI'),
    (u'Pasaporte', 'Pasaporte'),
    (u'Libreta Militar', 'Libreta Militar'),
)

TURNO_CHOICES = (
    ('M', 'Mañana'),
    ('T', 'Tarde'),
    ('N', 'Noche'),
)

PROCEDENCIA_CHOICES = (
    ('Mismo Centro', 'Mismo Centro'),
    ('Otro', 'Otro'),
) 
