# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from joshikousei.periodos.models import *


class Matricula(models.Model):
    alumno = models.ForeignKey(Alumno)
    periodo_seccion = models.ForeignKey(PeriodoSeccion, verbose_name="Sección", help_text="Para activar el Combo,debe elegir un Año,Nivel,Grado y Turno Correctos")
    #periodo_categoria = models.ForeignKey(PeriodoCategoria, verbose_name="Categoría")
    fecha_matricula = models.DateField(null=True, blank=True, default=date.today())
    estado = models.BooleanField("¿Matrícula Activada?", default=True)
    condicion = models.CharField("Condición", max_length=50, choices=CONDICION_CHOICES)
    codigo_modular = models.CharField("Cod. Modular", max_length=7, null=True, blank=True)
    procedencia = models.CharField("I.E. Proced.", max_length=50, null=True, blank=True)

    def __unicode__(self):
        return  u'%s - %s' % (self.periodo_seccion, self.alumno)
        
    def notas_alumnodos(self, psc):
        op = str(psc) + "---"
        return op
        
    def notas_alumno(self):
        from joshikousei.notas.models import Calificaciones, Criterios
        # notas_alumno
        #vvv = [
            #[[u'P', Decimal('12.00')], [u'O', Decimal('11.00')], [u'EX', Decimal('9.00')]],
            #[[u'P', Decimal('15.00')], [u'O', Decimal('6.00')], [u'EX', Decimal('12.00')]],
            #[[u'P', Decimal('0.00')], [u'O', Decimal('0.00')], [u'EX', Decimal('0.00')]],
            #[[u'P', Decimal('0.00')], [u'O', Decimal('0.00')], [u'EX', Decimal('0.00')]]
        #]
        try:
            obtener_notas = Calificaciones.objects.filter(alumno=self.id).order_by('criterio__unidad')
            periodo_seccion_curso = PeriodoSeccionCurso.objects.filter(periodo_seccion__id=self.periodo_seccion.id)
            lista_total = []
            for o in obtener_notas:
                lista_total.append(o)
                #unidad = o.criterio.unidad
                #criterios = Criterios.objects.filter(unidad__id=unidad.id)
                #for criterio in criterios:
                    #lista_total.append(criterio)
                #lista_total.append(criterios)
            
            #periodo_seccion_curso = obtener_notas[0].criterio.unidad.periodo_seccion_curso
            #unidades = Unidad.objects.filter(periodo_seccion_curso__id=periodo_seccion_curso.id)
            
            #lista_total = []
            #for unidad in unidades:
                #lista_unidad = []
                #for criterio in unidad.criterios():
                    #abreviatura = criterio.abreviatura
                    ##calificacion = obtener_notas.filter(criterio=criterio)
                    ##valor = calificacion[0].valor
                    ##calificacion = Calificaciones.objects.get(alumno=self.id, criterio=criterio)
                    #valor = Decimal("0.0")
                    #lista_unidad.extend([[abreviatura, valor]])
                #lista_total.append(lista_unidad)
        except:
            lista_total = None
        return lista_total
        # return [ [unidad1], [unidad2], [unidad3], [unidad4]  ]
        # return [ {'P':10, 'O':5, 'EX':12}, {'P':12, 'O':7, 'EX':11}, {'P':13, 'O':3, 'EX':10}, {'P':18, 'O':9, 'EX':17} ]
        
    class Meta:
        unique_together = (("alumno", "periodo_seccion"),)
        verbose_name = "matrícula"
        verbose_name_plural = "matrículas"