# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from joshikousei.matriculas.models import *

class MatriculaAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)

admin.site.register(Matricula, MatriculaAdmin) 
