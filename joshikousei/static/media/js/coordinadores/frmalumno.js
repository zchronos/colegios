$(document).ready(function() {

    var departamento = $('#id_departamento');
    var provincia = $('#id_provincia');
    var p = $('#id_provincia');

    var update_provincia = function() {
        
        if ($('#id_selected_dep').val()){
            var departamento_id = $('#id_selected_dep').val();
            departamento.val($('#id_selected_dep').val());
            $('#id_selected_dep').val('');
        }else{
            var departamento_id = departamento.val();
        }
        
        if ($('#id_selected_prov').val()){
            var selected_prov = $('#id_selected_prov').val();
            $('#id_selected_prov').val('');
        }else{
            var selected_prov = '';
        }
        
        provincia.empty();
        
        $.ajax({
            url: "provincias/"+departamento_id+"/",
            dataType: "json",
            success: function (data, status){
                if (data) {
                        $.each(data, function(i,item){
                            if ( item.pk == selected_prov){
                                var opcion = "<option value='" + item.pk + "' selected = 'selected'>" + item.fields.Provincia + "</option>";
                            }else{
                                var opcion = "<option value='" + item.pk + "'>" + item.fields.Provincia + "</option>";
                            }
                            provincia.append(opcion);
                        });
                }
                },
            error: function (data, status, e){
                var opcion = "<option value=''>Primero elija un departamento</option>";
                provincia.append(opcion);
                }
        });

    };

    update_provincia();
    
    departamento.change(function () {
        update_provincia();
    });

});
