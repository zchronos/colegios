# -*- coding: utf-8 -*-
from django import forms
from django.forms import fields, models, formsets, widgets
from django.conf import settings

class CriteriosForm(forms.Form):
    descripcion = forms.CharField(required=True, label="Descripción")
    abreviatura = forms.CharField(required=True, label="Abreviatura", widget=forms.TextInput(attrs={'size':'7'}))
    peso = forms.DecimalField(required=True, label="Peso", widget=forms.TextInput(attrs={'size':'7'}))
    
    class Media:
        js = (settings.ADMIN_MEDIA_PREFIX + "js/jquery.min.js",
            settings.ADMIN_MEDIA_PREFIX + "js/jquery.formset.js",
            )

CriteriosFormset = formsets.formset_factory(CriteriosForm)
EmptyCriteriosFormset = formsets.formset_factory(CriteriosForm, extra=0)



class UnidadForm(forms.Form):
    cantidad = forms.IntegerField(required=True, label="Cantidad")
    duplicar = forms.BooleanField(required=False)
    
    class Media:
        js = (settings.ADMIN_MEDIA_PREFIX + "js/jquery.min.js",
            )

