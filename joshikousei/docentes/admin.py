# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from joshikousei.docentes.models import *

class DocenteCursos(admin.TabularInline):
    model = DocenteCursos
    extra = 1
    
    
class DocenteAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    inlines = (DocenteCursos,)
    fieldsets = (
        ("Datos del Docente", {'fields':(
            'nombre', 'apellido_paterno', 'apellido_materno',
            )
            }),
    )
    
admin.site.register(Docente, DocenteAdmin)