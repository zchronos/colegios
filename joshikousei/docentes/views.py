# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.utils.safestring import mark_safe

from django.views.decorators.csrf import csrf_protect
from django.views.decorators.cache import never_cache

from django import forms
from django.forms import fields, models, formsets, widgets
from django.forms.util import ErrorList
from django.conf import settings
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.template import RequestContext
from decimal import Decimal

from joshikousei.docentes.models import *
from joshikousei.periodos.models import *
from joshikousei.matriculas.models import *
from joshikousei.notas.models import *
from joshikousei.alumnos.models import *
from joshikousei.ubicacion.models import Departamento
from joshikousei.alumnos.choices import GENDER_CHOICES, LENGUA_CHOICES
from joshikousei.local_apps.widgets import DateTimeWidget

from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from reportlab.platypus import Table, TableStyle


# FORMS

class CriteriosForm(forms.Form):
    descripcion = forms.CharField(required=True, label="Descripción")
    abreviatura = forms.CharField(required=True, label="Abreviatura", widget=forms.TextInput(attrs={'size':'7'}))
    peso = forms.DecimalField(required=True, label="Peso", widget=forms.TextInput(attrs={'size':'7'}))
    
    class Media:
        js = (settings.ADMIN_MEDIA_PREFIX + "js/jquery.min.js",
            settings.ADMIN_MEDIA_PREFIX + "js/jquery.formset.js",
            )

CriteriosFormset = formsets.formset_factory(CriteriosForm)
EmptyCriteriosFormset = formsets.formset_factory(CriteriosForm, extra=0)

class UnidadForm(forms.Form):
    cantidad = forms.IntegerField(required=True, label="Cantidad")
    duplicar = forms.BooleanField(required=False)
    
    class Media:
        js = (settings.ADMIN_MEDIA_PREFIX + "js/jquery.min.js",
            )

# VIEWS

def administrar_cursos(request):
    if request.user.is_authenticated():
        id_docente = request.user.id
        docente_cursos = DocenteCursos.objects.filter(docente__id=id_docente)
        return render_to_response("docentes/administrar_cursos.html", {"docente_cursos": docente_cursos, "user": request.user }, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('../../')

@csrf_protect
def add_unidad(request, periodo_seccion_curso_id):
    if request.user.is_authenticated():
        unidades = Unidad.objects.filter(periodo_seccion_curso__id = periodo_seccion_curso_id)
        if request.method == 'POST':
            form = UnidadForm(request.POST)
            # Validar que antes no existan registros
            # Crear unidades según la cantidad especificada
            if form.is_valid():
                if  unidades.count() == 0:
                    msg = False
                    c = form.cleaned_data['duplicar']
                    for n in range(form.cleaned_data['cantidad']):
                        u = Unidad(periodo_seccion_curso_id = periodo_seccion_curso_id, nombre = n+1, clonacion=c)
                        u.save()
                else:
                    msg = "No se han creado nuevas unidades porque ya existen, si desea crear nuevas unidades deberá eliminar las existentes"
                # mostrar tabla con las unidades
                unidades = Unidad.objects.filter(periodo_seccion_curso__id = periodo_seccion_curso_id)
                return render_to_response("docentes/add_unidad.html", {"msg": msg, "unidades": unidades, "user": request.user}, context_instance = RequestContext(request))
                #return HttpResponseRedirect('/docente/profile/administrarcursos/criterios/' + str(periodo_seccion_curso_id))
                
        else:
            form = UnidadForm()
        return render_to_response("docentes/add_unidad.html", {"unidades": unidades, "form": form, "user": request.user}, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('../../')
            
@csrf_protect
def add_criterios(request, unidad_id):
    if request.user.is_authenticated():
        unidad = Unidad.objects.get(id=unidad_id)
        # Revisar si ya existen criterios creados previamente para ésta unidad
        criterios = Criterios.objects.filter(unidad__id=unidad_id)
        if request.method == 'POST':
            formset = CriteriosFormset(request.POST)
            if formset.is_valid():
                modificado = False        # Variablo contol: Evita que al modificar la fórmula, si tiene activada la opción "clonar" ésta duplique registros
                if criterios.count() != 0:
                    modificado = True
                    criterios.delete()
                
                data = formset.cleaned_data
                for d in data:
                    try:
                        descripcion = d['descripcion']
                        abreviatura = d['abreviatura']
                        peso = d['peso']
                    except:
                        continue
                    # Duplico la fórmula si se cumple la condición
                    if unidad.clonacion is True:
                        unidades = Unidad.objects.filter(periodo_seccion_curso__id=unidad.periodo_seccion_curso.id)
                        if modificado is False:
                            for u in unidades:
                                criterio = Criterios(unidad_id=u.id, descripcion=descripcion, abreviatura=abreviatura, peso=peso)
                                criterio.save()
                        else:
                            for u in unidades:
                                u.clonacion = False
                                u.save()
                            criterio = Criterios(unidad_id=unidad.id, descripcion=descripcion, abreviatura=abreviatura, peso=peso)
                            criterio.save()
                    else:
                        criterio = Criterios(unidad_id=unidad.id, descripcion=descripcion, abreviatura=abreviatura, peso=peso)
                        criterio.save()
                curso_id = str(unidad.periodo_seccion_curso.id)
                return HttpResponseRedirect('../../'+ curso_id)
        else:
            cc = criterios.values('descripcion', 'abreviatura', 'peso')
            formset = CriteriosFormset(initial=cc)
        return render_to_response("docentes/add_criterios.html", {"unidad": unidad, "formset": formset, "user": request.user }, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('../../')
        
def add_calificaciones(request, periodo_seccion_curso_id):
    if request.user.is_authenticated():
        criterios_curso = Criterios.objects.filter(unidad__periodo_seccion_curso__id=periodo_seccion_curso_id)
        if criterios_curso.count() == 0:
            return render_to_response("docentes/error/no_formula.html", {"user": request.user }, context_instance = RequestContext(request))
            
        unidades = Unidad.objects.filter(periodo_seccion_curso__id=periodo_seccion_curso_id)
        periodoseccioncurso = PeriodoSeccionCurso.objects.get(id=periodo_seccion_curso_id)
        alumnos_matriculados = Matricula.objects.filter(periodo_seccion=periodoseccioncurso.periodo_seccion.id)
        
        lista_alumnos = []
        for alumno in alumnos_matriculados:
            lista_notas = []
            obtener_notas = Calificaciones.objects.filter(alumno__id=alumno.id, criterio__unidad__periodo_seccion_curso__id=periodoseccioncurso.id).order_by('criterio__unidad')
            #n = [ [alumno, [nota1, nota2, nota3]], [alumno, [nota1, nota2, nota3]] ]
            for nota in obtener_notas:
                lista_notas.append(nota)
                    
            lista_alumnos.extend([[alumno, lista_notas]])
        
        # Clave para los campos de texto en las calificaciones: [matricula_id - criterio_id]
        #alumnos_matriculados = Calificaciones.objects.filter(alumno__PeriodoSeccion__id=periodoseccioncurso.periodo_seccion.id)
        if request.method == 'POST':
            get_notas = dict(request.POST)
            for valor in get_notas:
                clave = valor.split("-")
                if len(clave) == 2:
                    matricula_id = clave[0]
                    criterio_id = clave[1]
                    nota = Decimal(str(get_notas[valor][0]).replace(",", "."))
                    # Verificar si dichos registros ya existen. En caso de no existir se crean
                    try:
                        g = Calificaciones.objects.get(alumno__id=matricula_id, criterio__id=criterio_id)
                        g.valor = nota
                        g.save()
                    except Calificaciones.DoesNotExist:
                        grabar_nota = Calificaciones(alumno_id=matricula_id, criterio_id=criterio_id, valor=nota)
                        grabar_nota.save()
            
            return HttpResponseRedirect('../../')
        
        return render_to_response("docentes/add_calificaciones.html", {"lista_alumnos": lista_alumnos, "unidades": unidades, "alumnos_matriculados": alumnos_matriculados, "user": request.user }, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('../../')
        
def reporte_alumnos(request):
    if request.user.is_authenticated():
        alumnos = Matricula.objects.all().order_by('alumno__apellido_paterno')
        return render_to_response("docentes/reporte_alumnos.html", {"alumnos": alumnos, "user": request.user}, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('../../')
        
def plantilla_infoalumno(c):
    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40

    c.setFont("Helvetica", 14)
    c.drawString(220, 800, "Información del Alumno")

    # Marco de la Página
    c.line(margenLeft, 22, margenLeft, 822)    # Linea Vertical Izquierda...
    c.line(anchoA4 - margenRight, 22, anchoA4 - margenRight, 822)    # Linea Vertical Derecha...
    c.line(margenLeft, 822, anchoA4 - margenRight, 822)    # Línea Horizontal Superior
    c.line(margenLeft, 22, anchoA4 - margenRight, 22)    # Línea Horizontal Inferior

    c.drawString(60, 590, "Familiares")
    c.line(margenLeft + 20, 580, anchoA4 - margenRight - 20, 580)    # Línea Horizontal

    c.drawString(60, 430, "Información de Contacto")
    c.line(margenLeft + 20, 420, anchoA4 - margenRight - 20, 420)    # Línea Horizontal
    
    return c
    
def nes(v):
    if v is True:
        return "SI"
    else:
        return "NO"
        
def generar_pdf(request, id_alumno):
    if request.user.is_authenticated():
        alumno = Alumno.objects.get(id=id_alumno)
        apoderados = Apoderado.objects.filter(representado__id=id_alumno)
        matricula = Matricula.objects.get(alumno__id=id_alumno)
        
        response = HttpResponse(mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=reporte_alumno.pdf'
        c = canvas.Canvas(response, pagesize = A4)
        c = plantilla_infoalumno(c)
        
        c.setFont("Helvetica", 12)
        c.drawString(200, 760, "Nombre: %s" % alumno.nombre)
        c.drawString(200, 740, "Apellido Paterno: %s" % alumno.apellido_paterno)
        c.drawString(200, 720, "Apellido Materno: %s" % alumno.apellido_materno)
        c.drawString(200, 700, "Fecha de Nacimiento: %s" % alumno.nacimiento)
        c.drawString(200, 680, unicode(matricula.periodo_seccion))
        
        # La foto debe de ser de 102x102 pixeles
        foto = unicode(alumno.foto.file)
        c.drawImage(foto, 50, 670)
        
        # Creación la primera tabla
        data = [['Nombre', 'Relación', '¿Vive?', '¿Tutor?']]
        
        valores = []
        for apoderado in apoderados:
            valores.extend([
                [unicode(apoderado), unicode(apoderado.relacion), nes(apoderado.vive), nes(apoderado.tutor)]
                ])
        data.extend(valores)
        t=Table(data)
        t.setStyle(TableStyle([('ALIGN', (1,1), (-1,-1), 'CENTER'),
                            ('FONTNAME', (0,0), (-1,0), 'Helvetica-Bold'),
                            ('VALIGN', (0,0), (0,-1), 'TOP'),
                            ('VALIGN', (0,-1), (-1,-1), 'MIDDLE'),
                            ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                            ]))
                            
        w,h = t.wrapOn(c,3*72,2*72)
        t.drawOn(c, 60, 570-h)
        
        # Creación de la segunda tabla
        info_contacto = ApoderadoContacto.objects.filter(apoderado__representado__id=id_alumno).order_by('apoderado__id')
        data = [['Apoderado', 'Descripción', 'Valor', 'Preferido']]
        
        valores = []
        for i, contacto in enumerate(info_contacto):
            apoderado = contacto.apoderado
            if i > 0 and apoderado == info_contacto[i-1].apoderado:
                apoderado = ""
            valores.extend([
                [unicode(apoderado), unicode(contacto.tipo), unicode(contacto.valor), nes(contacto.preferido)]
                ])
        data.extend(valores)
        t=Table(data)
        t.setStyle(TableStyle([('ALIGN', (1,0), (-1,-1), 'CENTER'),
                            ('FONTNAME', (0,0), (-1,0), 'Helvetica-Bold'),
                            ('VALIGN', (0,0), (0,-1), 'TOP'),
                            ('VALIGN', (0,-1), (-1,-1), 'MIDDLE'),
                            ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                            ]))

        w,h = t.wrapOn(c,3*72,2*72)
        t.drawOn(c, 60, 410-h)
        
        c.showPage()
        c.save()
        return response
        
    else:
        return HttpResponseRedirect('../../')