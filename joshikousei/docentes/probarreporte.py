# -*- coding: latin-1 -*-
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from reportlab.platypus import Table, TableStyle

c = canvas.Canvas("Hello.pdf", pagesize = A4)
# Por defecto se trabaja en 72dpi 72 ppp: 595x842
anchoA4 = 595
altoA4 = 842
margenLeft = 40
margenRight = 40

c.setFont("Helvetica", 14)
c.drawString(220, 800, "Información del Alumno")

# Marco de la Página
c.line(margenLeft, 22, margenLeft, 822)    # Linea Vertical Izquierda...
c.line(anchoA4 - margenRight, 22, anchoA4 - margenRight, 822)    # Linea Vertical Derecha...
c.line(margenLeft, 822, anchoA4 - margenRight, 822)    # Línea Horizontal Superior
c.line(margenLeft, 22, anchoA4 - margenRight, 22)    # Línea Horizontal Inferior

c.drawString(60, 590, "Familiares")
c.line(margenLeft + 20, 580, anchoA4 - margenRight - 20, 580)    # Línea Horizontal

c.drawString(60, 430, "Información de Contacto")
c.line(margenLeft + 20, 420, anchoA4 - margenRight - 20, 420)    # Línea Horizontal

# La foto debe de ser de 102x102 pixeles
foto = "/home/zchronos/male_default_user_thumb.png"
c.drawImage(foto, 50, 670)

c.setFont("Helvetica", 12)
c.drawString(200, 760, "Nombre:")
c.drawString(200, 740, "Apellido Paterno:")
c.drawString(200, 720, "Apellido Materno:")
c.drawString(200, 700, "Fecha de Nacimiento:")
c.drawString(200, 680, "1º de Primaria - A")

data= [['Nombre', 'Relación', '¿Vive?', '¿Tutor?'],
       ['Nombre10Nombre', '11', '12', '13'],
       ['20', '21', '22', '23'],
       ['30', '31', '32', '33']]

t=Table(data)
t.setStyle(TableStyle([('ALIGN', (1,1), (-1,-1), 'CENTER'),
                       ('FONTNAME', (0,0), (-1,0), 'Helvetica-Bold'),
                       ('VALIGN', (0,0), (0,-1), 'TOP'),
                       ('VALIGN', (0,-1), (-1,-1), 'MIDDLE'),
                       ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                       ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                       ]))
                       
w,h = t.wrapOn(c,3*72,2*72)
t.drawOn(c, 60, 570-h)

# Otra Tabla
data2= [['NombreApoderado', 'Descripción', 'Valor', 'Preferido'],
       ['Juan Pérez', 'Teléfono', '32125465', 'X'],
       ['', 'Celular', '32125465', 'X'],
       ['', 'Email', '32125465@asdf.com', 'X'],
       ['María Alcas', 'Teléfono', '32125465', 'X']]

t=Table(data2)
t.setStyle(TableStyle([('ALIGN', (1,0), (-1,-1), 'CENTER'),
                       ('FONTNAME', (0,0), (-1,0), 'Helvetica-Bold'),
                       ('VALIGN', (0,0), (0,-1), 'TOP'),
                       ('VALIGN', (0,-1), (-1,-1), 'MIDDLE'),
                       ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                       ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                       ]))

w,h = t.wrapOn(c,3*72,2*72)
t.drawOn(c, 60, 410-h)

c.showPage()
c.save()