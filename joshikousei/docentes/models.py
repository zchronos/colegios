# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from joshikousei.periodos.models import PeriodoSeccionCurso

class Docente(User):
    apellido_paterno = models.CharField("Ape. Pat.", max_length=50)
    apellido_materno = models.CharField("Ape. Mat.", max_length=50)
    nombre = models.CharField(max_length=50)
    
    def __unicode__(self):
        return unicode(self.nombre)
        
    def username(self):
        return self.username
    
    def add_grupo(self):
        from django.contrib.auth.models import Group
        # Agregar el docente al grupo, también
        # verificar que exista el grupo Docentes, si no existe, se crea
        try:
            grupo = Group.objects.get(name="Docentes")
            self.groups.add(grupo.id)
        except:
            grupo = Group(name="Docentes")
            grupo.save()
            self.groups.add(grupo.id)
            
    def save(self):
        import random
        if not self.user_ptr_id:
            nombre = self.nombre
            usuario = str(nombre[0] + self.apellido_paterno).lower()
            
            self.username = usuario
            self.set_password(usuario)
            self.first_name = nombre
            self.last_name = self.apellido_paterno + " " + self.apellido_materno
            #self.email = self.email
            super(Docente, self).save()
            return self.add_grupo()
        else:
            nombre = self.nombre
            self.first_name = nombre
            self.last_name = self.apellido_paterno + " " + self.apellido_materno
            #self.email = self.email
            super(Docente, self).save()
            return self.add_grupo()
    
class DocenteCursos(models.Model):
    docente = models.ForeignKey(Docente)
    periodo_seccion_curso = models.ForeignKey(PeriodoSeccionCurso)
    
    def __unicode__(self):
        return u'%s - %s' % (self.docente, self.periodo_seccion_curso)