# -*- coding: utf-8 -*-
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django import forms
import settings
import datetime, time
from django.utils.text import truncate_words


# DATETIMEWIDGET
calbtn = u"""<img src="%scalendario_admin/imagenbutton/calbutton.gif" alt="calendar" id="%s_btn" style="cursor: pointer;" title="Select a date " onmouseover="this.style.background='#ffffff';" onmouseout="this.style.background=''" />
<script type="text/javascript">
   var cal = Calendar.setup ({
            inputField: "%s",
            dateFormat: "%s",
            trigger: "%s_btn",
            bottomBar: false,
            onSelect: function() {
                  this.hide();
                          }
                  });
</script>"""

class DateTimeWidget(forms.widgets.TextInput):
    dformat = '%Y-%m-%d'
    def render(self, name, value, attrs=None):
        if value is None: value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '': 
            try:
                final_attrs['value'] = \
                                   force_unicode(value.strftime(self.dformat))
            except:
                final_attrs['value'] = \
                                   force_unicode(value)
        if not final_attrs.has_key('id'):
            final_attrs['id'] = u'%s_id' % (name)
        id = final_attrs['id']
        
        jsdformat = self.dformat
        cal = calbtn % (settings.ADMIN_MEDIA_PREFIX, id, id, jsdformat, id)
        a = u'<input%s />%s' % (forms.util.flatatt(final_attrs), cal)
        return mark_safe(a)

    def value_from_datadict(self, data, files, name):
        dtf = forms.fields.DEFAULT_DATETIME_INPUT_FORMATS
        empty_values = forms.fields.EMPTY_VALUES

        value = data.get(name, None)
        if value in empty_values:
            return None
        if isinstance(value, datetime.datetime):
            return value
        if isinstance(value, datetime.date):
            return datetime.datetime(value.year, value.month, value.day)
        for format in dtf:
            try:
                return datetime.datetime(*time.strptime(value, format)[:6])
            except ValueError:
                continue
        return None
        
class ForeignKeySearchInput(forms.HiddenInput):

    class Media:
        css = {
            'all': (settings.ADMIN_MEDIA_PREFIX + 'js/jquery_autocomplete/jquery.autocomplete.css',)
        }
        js = (settings.ADMIN_MEDIA_PREFIX + 'js/jquery_autocomplete/lib/jquery.js',
              settings.ADMIN_MEDIA_PREFIX + 'js/jquery_autocomplete/lib/jquery.bgiframe.min.js',
              settings.ADMIN_MEDIA_PREFIX + 'js/jquery_autocomplete/lib/jquery.ajaxQueue.js',
              settings.ADMIN_MEDIA_PREFIX + 'js/jquery_autocomplete/jquery.autocomplete.js'
        )

    def label_for_value(self, value):
        key = self.rel.get_related_field().name
        obj = self.rel.to._default_manager.get(**{key: value})
        return truncate_words(obj, 14)

    def __init__(self, rel, search_fields, attrs=None):
        self.rel = rel
        self.search_fields = search_fields
        super(ForeignKeySearchInput, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if attrs is None:
            attrs = {}
        rendered = super(ForeignKeySearchInput, self).render(name, value, attrs)
        if value:
            label = self.label_for_value(value)
        else:
            label = u''
        return rendered + mark_safe(u'''
            <style type="text/css" media="screen">
                #lookup_%(name)s {
                    padding-right:16px;
                    background: url(
                        %(admin_media_prefix)simg/admin/selector-search.gif
                    ) no-repeat right;
                }
                #del_%(name)s {
                    display: none;
                }
            </style>
<input type="text" id="lookup_%(name)s" value="%(label)s" />
<a href="#" id="del_%(name)s">
<img src="%(admin_media_prefix)simg/admin/icon_deletelink.gif" />
</a>
<script type="text/javascript">
            if ($('#lookup_%(name)s').val()) {
                $('#del_%(name)s').show();
            }
            $('#lookup_%(name)s').autocomplete('../search/', {
                extraParams: {
                    search_fields: '%(search_fields)s',
                    app_label: '%(app_label)s',
                    model_name: '%(model_name)s',
                },
            }).result(function(event, data, formatted) {
                if (data) {
                    $('#id_%(name)s').val(data[1]);
                    $('#del_%(name)s').show();
                }
            });
            $('#del_%(name)s').click(function(ele, event) {
                $('#id_%(name)s').val('');
                $('#del_%(name)s').hide();
                $('#lookup_%(name)s').val('');
            });
            </script>
        ''') % {
            'search_fields': ','.join(self.search_fields),
            'admin_media_prefix': settings.ADMIN_MEDIA_PREFIX,
            'model_name': self.rel.to._meta.module_name,
            'app_label': self.rel.to._meta.app_label,
            'label': label,
            'name': name,
        }
