# -*- coding: utf-8 -*-
from django.db import models


TIPO_CONTACTO = (
    (u'Dirección', 'Dirección'),
    (u'Teléfono', 'Teléfono'),
    (u'Celular', 'Celular'),
    (u'Fax', 'Fax'),
    (u'Email', 'Email'),
    (u'Nombre del Centro de Trabajo', 'Nombre del Centro de Trabajo'),
    (u'Dirección de Trabajo', 'Dirección de Trabajo'),
    (u'Teléfono de Trabajo', 'Teléfono de Trabajo'),
)

class Contacto(models.Model):
    tipo = models.CharField(max_length=13, choices=TIPO_CONTACTO)
    valor = models.CharField(max_length=200)
    preferido = models.BooleanField()
    
    def __unicode__(self):
        return u'%s(%s)' % (self.tipo, self.valor)