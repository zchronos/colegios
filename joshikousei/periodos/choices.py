# -*- coding: utf-8 -*-


# SECCION
NIVEL_CHOICES = (
    ('Inicial', 'Inicial'),
    ('Primaria', 'Primaria'),
    ('Secundaria', 'Secundaria'),
)

GRADO_CHOICES = (
    ('1', '1º'),
    ('2', '2º'),
    ('3', '3º'),
    ('4', '4º'),
    ('5', '5º'),
    ('6', '6º'),
)

TURNO_CHOICES = (
    ('M', 'Mañana'),
    ('T', 'Tarde'),
    ('N', 'Noche'),
)

MONEDA_CHOICES = (
    ('S/.', 'Nuevo Sol'),
    ('US$', 'Dólar'),
    ('€', 'Euro'),
)

CONDICION_CHOICES = (
    ('Promovido', 'Promovido'),
    ('Ingresante', 'Ingresante'),
    ('Traslado', 'Traslado'),
    ('Retirado', 'Retirado'),
    ('Reincorporado', 'Reincorporado'),
    ('Repetido', 'Repetido'),
)
