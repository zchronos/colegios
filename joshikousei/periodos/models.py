# -*- coding: utf-8 -*-
from django.db import models
from decimal import Decimal
from joshikousei.alumnos.models import Alumno
from joshikousei.periodos.choices import *
from datetime import date, datetime

#class Categoria(models.Model):
    #categoria = models.CharField("Categoría", max_length=50)
    #nivel = models.CharField("Nivel", max_length=20,choices=NIVEL_CHOICES)
    #moneda = models.CharField("Moneda", max_length=4,choices=MONEDA_CHOICES)
    #pago = models.DecimalField(max_digits=6, decimal_places=2, default="0.00", help_text="Ingrese el monto del Pago")

    #def __unicode__(self):
        #return u'%s %s - %s %s' % (self.categoria, self.nivel, self.moneda, str(self.pago))

    #def categoria(self):
        #return u'%s %s' % (self.categoria, self.nivel)

    #def pago(self):
        #return u'%s %s' % (self.moneda, str(self.pago))

    #class Meta:
        #verbose_name = "Categoría de Pago"
        #verbose_name_plural = "Categorías de Pago"

class Periodo(models.Model):
    comienza = models.DateField()
    termina = models.DateField()
    
    def __unicode__(self):
        return '%s - %s' % (self.comienza, self.termina)
    
    def anio(self):
        return unicode(self.comienza.year)

#class PeriodoCategoria(models.Model):
    #periodo = models.ForeignKey(Periodo)
    #categoria = models.ForeignKey(Categoria)

    #def __unicode__(self):
        #return  u'%s - %s' % (self.periodo, self.categoria)
    
    #class Meta:
        #unique_together = (("periodo", "categoria"),)
        #verbose_name = "Categoria por Periodo Academico"
        #verbose_name_plural = "Categorias por Periodo Academico"

class PeriodoSeccion(models.Model):
    periodo = models.ForeignKey(Periodo)
    nivel = models.CharField(max_length=10, choices=NIVEL_CHOICES)
    grado = models.CharField(max_length=2, choices=GRADO_CHOICES)
    seccion = models.CharField(max_length=20, blank=True, null=True)
    
    def __unicode__(self):
        return u'%s - %s - %s %s' % (self.periodo.anio(), self.nivel, self.grado, self.seccion)
        
    class Meta:
        verbose_name = "Periodo y Sección"
        verbose_name_plural = "Periodo y Sección"

    #def Hombres(self):
        #n_hombres = self.matricula_set.filter(Alumno__Sexo = 'M').count()
        #return n_hombres
    
    #def Mujeres(self):
        #n_mujeres = self.matricula_set.filter(Alumno__Sexo = 'F').count()
        #return n_mujeres
    
    #def NroAlumnos(self):
        #n_alumnos = self.matricula_set.count()
        #return n_alumnos

class PeriodoSeccionCurso(models.Model):
    periodo_seccion = models.ForeignKey(PeriodoSeccion)
    curso = models.CharField(max_length=30)
    estado = models.BooleanField("Activo", default=True)
    
    def __unicode__(self):
        return u'%s - %s' % (self.periodo_seccion, self.curso)
        
    def unidades(self):
        from joshikousei.periodos.models import Unidad
        try:
            total = Unidad.objects.filter(periodo_seccion_curso__id=self.id).count()
            return total
        except:
            return None

class Unidad(models.Model):
    periodo_seccion_curso = models.ForeignKey(PeriodoSeccionCurso)
    nombre = models.PositiveSmallIntegerField()
    clonacion = models.BooleanField(default=True)
    
    def __unicode__(self):
        return u'Unidad: %s' % (self.nombre)
        
    def formula(self):
        from joshikousei.notas.models import Criterios
        criterios = Criterios.objects.filter(unidad__id=self.id)
        lista = []
        denominador = Decimal("0.00")
        for criterio in criterios:
            denominador += criterio.peso
            lista.append(str(criterio.abreviatura) + "(" + str(criterio.peso) + ")")
        nominador = " + ".join(lista)
        if denominador == Decimal("0.00"):
            return ""
        else:
            return nominador + "<br /><hr>" + str(int(denominador))
            
    def criterios(self):
        from joshikousei.notas.models import Criterios
        criterios = Criterios.objects.filter(unidad__id=self.id)
        return criterios
        