# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from joshikousei.periodos.models import *

class PeriodoSeccionInline(admin.TabularInline):
    model = PeriodoSeccion
    extra = 1

#class PeriodoCategoriaInline(admin.TabularInline):
    #model = PeriodoCategoria
    #extra = 1
    
class PeriodoAdmin(admin.ModelAdmin):
    list_display = ('anio',)
    inlines = (PeriodoSeccionInline,)
    
admin.site.register(Periodo, PeriodoAdmin)

class PeriodoSeccionCurso(admin.TabularInline):
    model = PeriodoSeccionCurso
    extra = 1

class PeriodoSeccionAdmin(admin.ModelAdmin):
    list_display = ('get_anio', 'nivel', 'grado', 'seccion',)
    list_filter = ('nivel', 'grado',)
    inlines = (PeriodoSeccionCurso,)
    
    def get_anio(self, obj):
        return '%s'%(obj.periodo.comienza.year)
    get_anio.short_description = 'Año'
    
admin.site.register(PeriodoSeccion, PeriodoSeccionAdmin)

#class CategoriaAdmin(admin.ModelAdmin):
    #list_display = ('__unicode__',)

#admin.site.register(Categoria, CategoriaAdmin)

#class PeriodoCategoriaAdmin(admin.ModelAdmin):
    #list_display = ('__unicode__',)

#admin.site.register(PeriodoCategoria, PeriodoCategoriaAdmin)
