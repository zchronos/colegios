# -*- coding: utf-8 -*-
from django import forms
from django.forms import fields, models, formsets, widgets
from django.contrib.admin.widgets import AdminDateWidget
from django.conf import settings
from joshikousei.periodos.models import Criterios


CONTACT_INFO_TYPES = (
    ('Phone', 'Phone'),
    ('Fax', 'Fax'),
    ('Email', 'Email'),
    ('AIM', 'AIM'),
    ('Gtalk', 'Gtalk/Jabber'),
    ('Yahoo', 'Yahoo'),
)

class ContactInfoForm(forms.Form):
    type = fields.ChoiceField(choices=CONTACT_INFO_TYPES)
    value = fields.CharField(max_length=200)
    preferred = fields.BooleanField(required=False)

ContactFormset = formsets.formset_factory(ContactInfoForm)