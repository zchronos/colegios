# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
admin.autodiscover()
from local_apps.authentication import *
from joshikousei.docentes.views import *
from joshikousei.coordinadores.views import *
from joshikousei.ubicacion.views import *
from django.views.generic import TemplateView


urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'joshikousei.home.index', name='index'),
    # url(r'^joshikousei/', include('joshikousei.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    # urls para intranet directores, docentes, etc
    (r'^j/login/$', login_saie),
    (r'^j/logout/$', logout_saie),
    (r'^j/cambiar_passwd/$', cambiar_passwd),
    
    (r'^j/profile/$', index_intranet),
    
    # Ubicación
    url(r'provincias/(?P<departamento_id>\d*)/$', provincia, name='provincia'),
    
    
    (r'^j/profile/docentes/$', index_docentes),
    (r'^j/profile/docentes/administrarcursos/$', administrar_cursos),
    (r'^j/profile/docentes/administrarcursos/(\d+)/$', add_unidad),
    (r'^j/profile/docentes/administrarcursos/addcriterios/(\d+)/$', add_criterios),
    (r'^j/profile/docentes/administrarcalificaciones/$',  TemplateView.as_view(template_name='docentes/add_calificaciones.html')),
    (r'^j/profile/docentes/administrarasistencias/$',  TemplateView.as_view(template_name='docentes/add_asistencias.html')),
    (r'^j/profile/docentes/administrarcalificaciones/calificaciones/(\d+)/$', add_calificaciones),
    (r'^j/profile/docentes/reporte_alumnos/$', reporte_alumnos),
    #(r'^j/profile/docentes/reporte_alumnos/(\d+)/$', generar_pdf),
    
    (r'^j/profile/coordinadores/$', index_coordinadores),
    (r'^j/profile/coordinadores/buscar_alumno/$', buscar_alumno),
    (r'^j/profile/coordinadores/add_alumno/$', add_alumno),
    (r'^j/profile/coordinadores/add_docente/$', add_docente),
    #(r'^j/profile/coordinadores/add_alumno/(\d+)/$', add_alumno),    
    ##(r'^j/profile/coordinadores/mod_alumno/(\d+)/$', mod_alumno),
)
