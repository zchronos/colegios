# -*- coding: utf-8 -*-
from django.db import models
from joshikousei.periodos.models import *
from joshikousei.matriculas.models import *

class Criterios(models.Model):
    unidad = models.ForeignKey(Unidad)
    descripcion = models.CharField(max_length=40)
    abreviatura = models.CharField(max_length=4)
    peso = models.DecimalField(max_digits=5, decimal_places=2)
    
    def __unicode__(self):
        return u'%s(%s)' % (self.abreviatura, self.peso)

class Calificaciones(models.Model):
    alumno = models.ForeignKey(Matricula)
    criterio = models.ForeignKey(Criterios)
    vencimiento = models.DateTimeField(blank=True, null=True)
    edicion = models.BooleanField(default=True)
    valor = models.DecimalField(max_digits=5, decimal_places=2, default="0.00", help_text="Valor de la nota")
    
    ## En la vista utilizar JavaScrip para mostrar un checkbox ("Bloqueo por fecha") en caso de marcarlo,
    ## el campo "Edicion" desaparece y se muestra un campo de fecha (así la nota se bloquea solamente por fecha). Caso contrario se muestra el checkbox
    ## "Edicion" para activar o desactivar el ingreso de calificaciones.
    #def save(self):
        #if self.vencimiento != None:
            #hoy = datetime.today()
            #if hoy > self.vencimiento:
                #self.edicion = True
            #else:
                #self.edicion = False
        #return super(Calificaciones, self).save()
        
    def __unicode__(self):
        return u'%s - %s - %s' % (self.alumno, self.criterio, self.valor)