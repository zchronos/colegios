# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.utils.safestring import mark_safe

from django.views.decorators.csrf import csrf_protect
from django.views.decorators.cache import never_cache

from django import forms
from django.forms import fields, models, formsets, widgets
from django.forms.util import ErrorList
from django.conf import settings
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.template import RequestContext
from decimal import Decimal

from joshikousei.docentes.models import *
from joshikousei.periodos.models import *
from joshikousei.matriculas.models import *
from joshikousei.notas.models import *
from joshikousei.alumnos.models import *
from joshikousei.ubicacion.models import Departamento
from joshikousei.alumnos.choices import GENDER_CHOICES, LENGUA_CHOICES
from joshikousei.local_apps.widgets import DateTimeWidget

from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from reportlab.platypus import Table, TableStyle

# FORMS
class ApoderadoForm(forms.Form):
    apoderado = forms.ModelChoiceField(queryset=Apoderado.objects.all(), widget=forms.Select(), required=True, label="Apoderado")
    relacion = forms.ChoiceField(choices=RELACION_CHOICES, required=True, label="Relación")
    tutor = forms.BooleanField(required=False, label="Tutor")
    
    class Media:
        js = (settings.ADMIN_MEDIA_PREFIX + "js/jquery.min.js",
            settings.ADMIN_MEDIA_PREFIX + "js/jquery.formset.js",
            )

ApoderadoFormset = formsets.formset_factory(ApoderadoForm)
EmptyApoderadoFormset = formsets.formset_factory(ApoderadoForm, extra=0)

class Alumno_Form(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(Alumno_Form, self).__init__(*args, **kwargs)
        try:
            for field in self:
                id_provincia = field.form.initial['provincia']
                break
            self.fields['selected_dep'].initial = Departamento.objects.get(provincia__id=id_provincia).id
            self.fields['selected_prov'].initial = id_provincia
            #self.fields['departamento'].initial = Departamento.objects.get(provincia__id=id_provincia).id
            #self.fields['provincia'].initial = id_provincia
        except:
            pass
    
    nacimiento = forms.DateField(widget= DateTimeWidget)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all().order_by('Departamento'), widget=forms.Select(), required=False)
    selected_dep = forms.CharField(widget=forms.HiddenInput, required=False,label="")
    selected_prov = forms.CharField(widget=forms.HiddenInput, required=False,label="")
    
    class Meta:
        model = Alumno
        
    class Media:
        js = (
            settings.ADMIN_MEDIA_PREFIX + "js/jquery.min.js",
            settings.ADMIN_MEDIA_PREFIX + "js/coordinadores/frmalumno.js",
            settings.ADMIN_MEDIA_PREFIX + "calendario_admin/src/js/jscal2.js",
            settings.ADMIN_MEDIA_PREFIX + "calendario_admin/src/js/lang/es.js",
            )
        css = {
               "all": (settings.ADMIN_MEDIA_PREFIX + "calendario_admin/src/css/jscal2.css",
               settings.ADMIN_MEDIA_PREFIX + "calendario_admin/src/css/border-radius.css",
               settings.ADMIN_MEDIA_PREFIX + "calendario_admin/src/css/reduce-spacing.css",
               settings.ADMIN_MEDIA_PREFIX + "calendario_admin/src/css/steel/steel.css",)
               }

class Docente_Form(forms.ModelForm):
    class Meta:
        model = Docente
        
# VIEWS

@csrf_protect
def buscar_alumno(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            action = request.POST['action']
            all = request.POST['select_across']
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')
            
            #if all == '0':
                #alumnos_seleccinados = request.POST.getlist('_selected_action')
            #else:
                #alumnos_seleccinados = []
                #ids_mat = Alumno.objects.filter()
                ##if query1:
                    ##ids_mat1 = ids_mat.filter(Alumno__Carrera__id = query1)
                ##else:
                    ##ids_mat1 = ids_mat
                ##if query2:
                    ##ids_mat2 = ids_mat1.filter(Alumno__AnioIngreso = query2)
                ##else:
                    ##ids_mat2 = ids_mat1
                ##if query3:
                    ##ids_mat3 = ids_mat2.filter(Alumno__Semestre = query3)
                ##else:
                    ##ids_mat3 = ids_mat2
                ##if query4:
                    ##ids_mat4 = ids_mat3.filter(Estado = query4)
                ##else:
                    ##ids_mat4 = ids_mat3
                    
                #for id_mat in ids_mat4:
                    #alumnos_seleccinados.append(id_mat.id)
            #alumnos_carnet = []
            #matriculados = []
            
            #for mat in alumnos_seleccinados:
                #alumno = MatriculaCiclo.objects.get(id = mat)
                #alumnos_carnet.append(alumno)

            #for mat in alumnos_seleccinados:
                #cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id = mat)
                #matriculados.append(cursos_matriculado)
            ##if action == "print_matricula_selected":
                ##return print_ficha_matricula(matriculados)
            ##elif action == "print_notas_selected":
                ##return print_ficha_notas(matriculados)
            ##elif action == "print_notas1_selected":
                ##return print_ficha_notas1(matriculados)
            ##elif action == "promedios_alumnos_selected":
                ##return archivo_promedio_cursos(matriculados)
            ##elif action == "promedios_ponderados_alumnos_selected":
                ##return archivo_promedio_ponderado(matriculados)
            ##elif action == "print_carnet_selected":
                ##return print_carnet_estudiantes(alumnos_carnet)
            ##elif action == "print_asistencia_selected":
                ##return print_reporte_asistencia(matriculados)
            ##elif action == "print_reporte_asistencias_selected":
                ##return print_reporte_asistencias_pdf(matriculados)
            ##elif action == "print_reporte_notas_selected":
                ##return print_reporte_notas(matriculados)
            ##elif action == "print_certificado_estudios_selected":
                ##return print_certificado_estudios(alumnos_seleccinados)
            ##elif action == "print_historial_academico_selected":
                ##return print_historial_academico(alumnos_seleccinados)
            ##elif action == "print_datos_matriculados_selected":
                ##return datos_matriculados_excel(alumnos_carnet)
            ##elif action == "print_categorias_matriculados_selected":
                ##return listado_categorias_excel(alumnos_carnet)
            ##else:
                ##return HttpResponseRedirect('../../../../../../')
        else:
            query = request.GET.get('q','')
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')
            
            #carreras = Carrera.objects.all()
            anios = []
            genero = GENDER_CHOICES
            #semestres = ['I','II','III']
            #condiciones = ['Matriculado',u'Reserva de Matrícula','Retirado']

            for i in range(2010,2022):
                anios.append(i)

            #try:
                #periodo = Periodo.objects.get(id = idperiodo)
            #except Periodo.DoesNotExist:
                #mensaje = "El periodo elegido no es correcto"
                #links = "<a href='../../../'>Volver</a>"
                #return render_to_response("Pagos/mensaje.html", {"mensaje": mensaje,"links": mark_safe(links),"user": request.user })
                
            #results = MatriculaCiclo.objects.filter(Periodo = periodo)
            results = Alumno.objects.all()
            
            #if query1:
                #results1 = results.filter(Alumno__Carrera__id = query1)
            #else:
                #results1 = results
            #if query2:
                #results2 = results1.filter(Alumno__AnioIngreso = query2)
            #else:
                #results2 = results1
            #if query3:
                #results3 = results2.filter(Alumno__Semestre = query3)
            #else:
                #results3 = results2
            #if query4:
                #results4 = results3.filter(Estado = query4)
            #else:
                #results4 = results3
            results4 = results
            if query:
                str_q_nombres = "Q(nombre__icontains = "
                str_q_apellidopaterno = "Q(apellido_paterno__icontains = "
                str_q_apellidomaterno = "Q(apellido_materno__icontains = "
                lista_query = query.split(" ")
                g = ""
                for i in lista_query:
                    g = g + "Q(nombre__icontains = \"%s\")|Q(apellido_paterno__icontains = \"%s\")|Q(apellido_materno__icontains = \"%s\")|" % (i, i, i)
                    
                qset = (
                        Q(codigo_educando__icontains = query)|
                        eval(g[:-1])
                )
                
                results5 = results4.filter(qset)
                #results5 = results4.filter(qset).order_by('Alumno__Carrera__Carrera','Alumno__ApellidoPaterno','Alumno__ApellidoMaterno','Alumno__Nombres')
            else:
                results5 = results4
                #results5 = results4.order_by('Alumno__Carrera__Carrera','Alumno__ApellidoPaterno','Alumno__ApellidoMaterno','Alumno__Nombres')
                
            n_alumnos = results5.count()
            paginator = Paginator(results5, 50)
            # Make sure page request is an int. If not, deliver first page.
            try:
                    page = int(request.GET.get('page', '1'))
            except ValueError:
                    page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                    results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                    results = paginator.page(paginator.num_pages)

            return render_to_response("coordinadores/buscar_alumno.html", {"results": results, "paginator": paginator,
                "query": query,"query1": query1, "query2": query2, "query3": query3, "query4": query4,
                "genero": genero,
                "anios": anios, "n_alumnos": n_alumnos, "user": request.user }, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../../')
        
@csrf_protect
def add_alumno(request, id_alumno=0):
    if request.user.is_authenticated():
        alumno = Alumno.objects.filter(id=id_alumno)
        alumnoapoderado = AlumnoApoderado.objects.filter(alumno=alumno)
        if request.method == 'POST':
            form = Alumno_Form(request.POST)
            formset_apoderado = ApoderadoFormset(request.POST, prefix='apoderado_form')
            if form.is_valid() and formset_apoderado.is_valid():
                apellido_paterno = form.cleaned_data['apellido_paterno']
                apellido_materno = form.cleaned_data['apellido_materno']
                nombre = form.cleaned_data['nombre']
                sexo = form.cleaned_data['sexo']
                nacimiento = form.cleaned_data['nacimiento']
                provincia = form.cleaned_data['provincia']
                distrito = form.cleaned_data['distrito']
                lengua_materna = form.cleaned_data['lengua_materna']
                religion = form.cleaned_data['religion']
                
                grabar_alumno = Alumno(apellido_paterno=apellido_paterno, apellido_materno=apellido_materno, nombre=nombre, sexo=sexo,
                    nacimiento=nacimiento, provincia=provincia, distrito=distrito, lengua_materna=lengua_materna, religion=religion)
                grabar_alumno.save()
                
                data_apoderado = formset_apoderado.cleaned_data
                for d in data_apoderado:
                    grabar_apoderado = AlumnoApoderado(alumno_id=grabar_alumno.id, apoderado_id=d['apoderado'].id, relacion=d['relacion'], tutor=d['tutor'])
                    grabar_apoderado.save()
                return HttpResponseRedirect('../buscar_alumno/')
        else:
            try:
                cc1 = alumno.values('foto', 'apellido_paterno', 'apellido_materno', 'observaciones', 'provincia', 'dni', 'sexo', 'religion', 'codigo_educando',
                    'nombre', 'nacimiento', 'lengua_adicional', 'distrito', 'lengua_materna')[0]
                cc2 = alumnoapoderado.values('apoderado', 'relacion', 'tutor')
            except:
                cc1 = []
                cc2 = []
            form = Alumno_Form(initial=cc1)
            formset_apoderado = ApoderadoFormset(prefix='apoderado_form', initial=cc2)
        return render_to_response("coordinadores/add_alumno.html", {"form": form, "formset_apoderado": formset_apoderado, "user": request.user }, context_instance = RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("mensaje.html", {"mensaje": mensaje,"links": mark_safe(links),"user": request.user })
        
@csrf_protect
def add_docente(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = Docente_Form(request.POST)
            if form.is_valid():
                apellido_paterno = form.cleaned_data['apellido_paterno']
                apellido_materno = form.cleaned_data['apellido_materno']
                nombre = form.cleaned_data['nombre']
                return HttpResponseRedirect('../buscar_alumno/')
                #grabar_docente = Docente(apellido_paterno=apellido_paterno, apellido_materno=apellido_materno, nombre=nombre)
                #grabar_docente.save()
        else:
            form = Docente_Form()
        return render_to_response("coordinadores/add_docente.html", {"form": form, "user": request.user }, context_instance = RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("mensaje.html", {"mensaje": mensaje,"links": mark_safe(links),"user": request.user })
        