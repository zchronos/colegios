from os import path
import Image
from django.contrib import admin
from joshikousei.anuncios.models import anuncios

class AnunciosAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
admin.site.register(anuncios, AnunciosAdmin)