from django.db import models

# Create your models here.
    
class anuncios(models.Model):
    nombre = models.CharField(max_length=128)
    descripcion = models.TextField()
    fecha = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name = "Anuncio"
        verbose_name_plural = "Anuncios"