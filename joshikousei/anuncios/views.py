# Create your views here.
from django.shortcuts import render_to_response
from joshikousei.anuncios.models import anuncios

#los anuncios son los eventos que van a ocurrir

def get_anuncios(request):
    lista_anuncios = anuncios.objects.all()
    return render_to_response('anuncios/index_anuncios.html', {'lista_anuncios' : lista_anuncios })
